import { configureStore } from '@reduxjs/toolkit'
import { useDispatch } from "react-redux";
import userReducer from "./userReducer";
import netReducer from "./net/netReducer";
import lobbyReducer from "./pages/lobby/lobbyReducer";

export const store = configureStore({
  reducer: {
    user: userReducer,
    net: netReducer,
    lobby: lobbyReducer
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch;
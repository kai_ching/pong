import { ThemeProvider } from '@mui/material';
import React, { useEffect } from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes, useNavigate } from "react-router-dom";
import Lobby from './pages/lobby/Lobby';
import Login from './pages/login/Login';
import Room from './pages/Room';
import theme from './theme';
import { Provider } from "react-redux";
import { store, useAppDispatch } from "./store";
import socket from "./net/socket";
import { ConnectionStatus, setConnectionStatus } from "./net/netReducer";
import { Actions, LobbyChanges, Message, MessageTypes, RoomStatus } from "@curiosity/pong-common";
import { addRoom, removeRoom, setLobby, updateRoom } from "./pages/lobby/lobbyReducer";

const Themed = ({ children } ) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
);

const WebsocketMonitor = (component) => {

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    socket.addEventListener('connecting', () => {
      dispatch(setConnectionStatus(ConnectionStatus.CONNECTING));
    });
    socket.addEventListener('disconnected', () => {
      dispatch(setConnectionStatus(ConnectionStatus.DISCONNECTED));
      history.pushState({}, "",'/');
    });

    socket.listenFor(MessageTypes.ROOM_STATUS, (message: Message<RoomStatus>) => {
      if(message.senderId?.charAt(0) ==='_') {
        dispatch(setLobby(message.payload!));
        navigate('/lobby');
      } else {
        dispatch(updateRoom({
          id: message.senderId,
          members: message.payload?.members!,
          rooms: []
        }));
      }
      return false;
    });
    socket.listenFor(MessageTypes.LOBBY_UPDATES, (message: Message<LobbyChanges[]>) => {
      message.payload!.forEach(change => {
        const { action, room} = change;
        switch(action) {
          case Actions.ADD: {
            dispatch(addRoom(room.id));
            break;
          }
          case Actions.REMOVE: {
            dispatch(removeRoom(room.id));
            break;
          }
        }
      });
      return false;
    });
  }, []);
  return component;
}

const PageWrapper = ({page} ) => (
  WebsocketMonitor(
    <Themed>
      { page }
    </Themed>
  )
);

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={ <PageWrapper page={<Login/>} /> } />
          <Route path="lobby" element={ <PageWrapper page={<Lobby/>} /> } />
          <Route path="room/:roomId" element={ <PageWrapper page={<Room/>} /> } />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  )
};

const withReduxProvider = (app) => (
  <Provider store={store}>
    {app}
  </Provider>
);

ReactDOM.render(withReduxProvider(<App />), document.getElementById('app'));

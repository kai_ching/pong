import { createAction, createReducer, PayloadAction } from "@reduxjs/toolkit";


const initialState = {
  name: ''
}

export const setName = createAction<string>('SET_NAME');

export default createReducer(initialState, builder => {
  builder.addCase(setName.type, (state = initialState, action: PayloadAction<string>) => {
    state.name = action.payload;
  });
})
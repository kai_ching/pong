import { Box, Button, TextField } from "@mui/material";
import React, { FormEvent, useState } from "react";
import { RootState, useAppDispatch } from "../../store";
import { setName } from "../../userReducer";
import socket from "../../net/socket";
import { useSelector } from "react-redux";
import { ConnectionStatus, setConnectionStatus, setServerId } from "../../net/netReducer";
import { joinMessage, Role } from "@curiosity/pong-common";

type BannerProps = {
  title: string;
  subTitle?: string;
}

const Banner = ({title, subTitle}: BannerProps) => {
  return (
    <Box display="flex" justifyContent="center">
      <h1 style={{marginTop:0}}>{title}</h1>
      { subTitle ? <h3>{subTitle}</h3> : null }
    </Box>
  )
}


export default () => {
  const dispatch = useAppDispatch();
  const [formName, setFormName] = useState('');

  const onFieldChange = (event: FormEvent<HTMLInputElement|HTMLTextAreaElement>) => {
    setFormName(event.currentTarget.value);
  }

  const onSubmit = event => {
    event.preventDefault();
    dispatch(setName(formName));

    socket.addEventListener('connected', (serverMessage) => {
      dispatch(setConnectionStatus(ConnectionStatus.CONNECTED));
      dispatch(setServerId(serverMessage?.senderId!));

      const globalRoomId = serverMessage?.senderId;
      const message = joinMessage(globalRoomId!, formName, Role.SPECTATOR);
      socket.send(message);
    });
    socket.connect("ws://localhost:3000/realtime");
  }

  const formValid = formName && formName.length > 0;
  const connectionStatus = useSelector((state: RootState) => state.net.status);

  return (
    <Box height="100%" display="flex" justifyContent="center" alignItems="center">
      <div>
        <Banner title="Pong"/>
        <form onSubmit={onSubmit}>
          <Box display="flex" gap={2} minWidth={350}>
            <TextField disabled={connectionStatus != ConnectionStatus.DISCONNECTED}
              fullWidth label="What's your name?" onChange={onFieldChange}/>
            <Button disabled={connectionStatus !== ConnectionStatus.DISCONNECTED || !formValid} color="primary" variant="contained" type="submit">
              Go
            </Button>
          </Box>
        </form>
      </div>
    </Box>
  );
};

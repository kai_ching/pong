import { createAction, createReducer, PayloadAction } from "@reduxjs/toolkit";
import { LobbyUser, RoomStatus, RoomSummary } from "@curiosity/pong-common";

const initialState = {
  members: [] as LobbyUser[],
  rooms: [] as RoomSummary[]
}

export type RoomPlayer = {
  roomId: string;
  playerId: string;
}

export const setLobby = createAction<RoomStatus>('SET_LOBBY');
export const addRoom = createAction<string>('ADD_ROOM');
export const removeRoom = createAction<string>('REMOVE_ROOM');
export const updateRoom = createAction<RoomStatus>('UPDATE_ROOM');

export default createReducer(initialState, builder => {
  builder.addCase(setLobby.type, (state, action: PayloadAction<RoomStatus>) => {
    state = action.payload;
    return state;
  });
  builder.addCase(addRoom.type, (state, action: PayloadAction<string>) => {
    state.rooms.push({
      id: action.payload,
      size: 0,
      members: []
    });
    return state;
  });
  builder.addCase(removeRoom.type, (state, action: PayloadAction<string>) => {
    const index = state.rooms.findIndex(room => room.id = action.payload);
    if(index > -1) {
      state.rooms.splice(index, 1);
    }
    return state;
  });
  builder.addCase(updateRoom.type, (state, action: PayloadAction<RoomStatus>) => {
    const { id, members } = action.payload;
    const index = state.rooms.findIndex(room => room.id === id);
    if(index > -1) {
      state.rooms[index].members = members;
    }
    return state;
  })
})
import React, { useEffect } from "react";
import { Box } from "@mui/material";
import { useSelector } from "react-redux";
import { RootState } from "../../store";
import RoomTile from "./RoomTile";
import { openRoomMessage } from "@curiosity/pong-common";
import socket from "../../net/socket";
import { ConnectionStatus } from "../../net/netReducer";
import { useNavigate } from "react-router-dom";

const CreateRoom = () => {
  return (
    <Box aria-label="Create room"
         mt={1} mb={1}
         p={1} display="flex"
         justifyContent="center" alignItems="center"
         border="1px dashed #aaa"
         height={150}
         style={{cursor: "pointer"}}
         onClick={() => {
           socket.send(openRoomMessage());
         }}
    >
      <span>Create Room</span>
    </Box>
  )
}

export default () => {
  const rooms = useSelector((state: RootState) => state.lobby.rooms);
  const connectionStatus = useSelector((state: RootState) => state.net.status);

  const navigate = useNavigate();
  useEffect(() => {
    if(connectionStatus === ConnectionStatus.DISCONNECTED) {
      navigate('/');
    }
  }, [connectionStatus]);

  return (
    <Box display="grid"
         gridTemplateColumns="repeat(3, 1fr)"
         columnGap={3}
         rowGap={3}
         m={3}
    >
      { rooms.map(room => <RoomTile key={room.id} room={room}/>) }
      <CreateRoom/>
    </Box>
  )
}

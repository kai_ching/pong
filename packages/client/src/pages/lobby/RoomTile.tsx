import React from 'react';
import { Box, Button, Paper } from "@mui/material";
import { joinMessage, Role } from "@curiosity/pong-common";
import socket from "../../net/socket";
import { useSelector } from "react-redux";
import { RootState } from "../../store";

type JoinButtonProps = {
  roomId: string;
  role: Role;
}
const JoinButton = ({ roomId, role }: JoinButtonProps) => {
  const name = useSelector((state: RootState) => state.user.name);
  return (
    <Box component="span" ml={1}>
      <Button size="small" variant="contained" color="primary"
              onClick={() => {
                socket.send(joinMessage(roomId, name, role), roomId);
              }}
      >
        Join
      </Button>
    </Box>
  )
}
export default ({room}) => {

  const members = room.members ?? [];

  const player1 = members.find(member => member.role === Role.PLAYER_LEFT);
  const player2 = members.find(member => member.role === Role.PLAYER_RIGHT);

  return (
    <Paper>
      <Box p={2} height={150}>
        <Box mb={2}>
          <Box component="span" mr={2}>Player 1:</Box>
          { player1 ? <span>{player1.name}</span> : <JoinButton roomId={room.id} role={Role.PLAYER_LEFT}/> }
        </Box>
        <Box mb={2}>
          <Box component="span" mr={2}>Player 2:</Box>
          { player2 ? <span>{player2.name}</span> : <JoinButton roomId={room.id} role={Role.PLAYER_RIGHT}/> }
        </Box>
      </Box>
    </Paper>
  );
}
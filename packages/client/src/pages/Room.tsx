import React from 'react';
import { useParams } from 'react-router-dom';

export default () => {
  const { roomId } = useParams<{ roomId: string }>();
  return <div>I am room {roomId}</div>;
};

import { Message, MessageTypes, ServerHello } from "@curiosity/pong-common";

type EventType = 'connecting' | 'connected' | 'disconnected';
type MessageHandler = (message: Message<any>) => boolean;

const noop = () => {};

const getHandlerKey = (type: MessageTypes) : string => type.toString();

class Client {

  private ws?: WebSocket;
  private sessionId?: string;
  private serverId?: string;

  private onConnecting: () => void = noop;
  private onConnected: (message: Message<ServerHello>) => void = noop;
  private onDisconnected: () => void = noop;

  private messageHandlers: { [key: string] : MessageHandler[]} = {};

  constructor() {

  }

  addEventListener(eventType: EventType, handler: (message?: Message<any>) => void) {
    switch(eventType) {
      case 'connecting': this.onConnecting = handler; break;
      case 'connected' : this.onConnected = handler; break;
      case 'disconnected' : this.onDisconnected = handler; break;
    }
  }

  listenFor(type: MessageTypes, handler: MessageHandler) {
    const key = getHandlerKey(type);
    let handlers = this.messageHandlers[key];
    if(!handlers) {
      handlers = [];
      this.messageHandlers[key] = handlers;
    }
    handlers.push(handler);
  }

  stopListen(type: MessageTypes, handler: MessageHandler) {
    const key = getHandlerKey(type);
    let handlers = this.messageHandlers[key];
    if(handlers && handlers.length > 0) {
      const index = handlers.indexOf(handler);
      if(index > -1) {
        handlers.splice(index, 1);
      }
    }
  }

  private dispatchMessage(message: Message<any>) {
    console.log('message type:', message.type)
    const handlers = this.messageHandlers[getHandlerKey(message.type)];
    if(handlers && handlers.length > 0) {
      handlers.some(handler => handler(message));
    }
  }

  connect(url: string) {
    this.onConnecting();

    try {
      this.ws = new WebSocket(url);
      this.ws.addEventListener('open', (event) => {
        this.ws!.addEventListener('message', (event) => {
          const message = Message.unpack(event.data);

          if(message.type === MessageTypes.SERVER_HELLO) {
            this.sessionId = message.recipientId;
            this.serverId = message.senderId!;
            this.onConnected(message as Message<ServerHello>);
          } else {
            this.dispatchMessage(message);
          }
        });
      });
    } catch(error) {
      this.onDisconnected();
    }
  }

  send(message: Message<any>, recipientId: string = this.serverId!) {
    if(this.ws && this.ws.readyState === 1) {
      this.ws.send(message.pack(recipientId, this.sessionId!));
    }
  }

  disconnect() {
    if(this.ws && this.ws.readyState === 1) {
      this.ws.close();
      this.onDisconnected();
    }
  }
}

export default new Client();
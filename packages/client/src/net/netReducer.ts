import { createAction, createReducer, PayloadAction } from "@reduxjs/toolkit";

export enum ConnectionStatus {
  DISCONNECTED,
  CONNECTING,
  CONNECTED
}

const initialState = {
  status: ConnectionStatus.DISCONNECTED,
  serverId: null as unknown as string,
  sessionId: null
}

export const setConnectionStatus = createAction<ConnectionStatus>('SET_CONNECTION_STATUS');
export const setServerId = createAction<string>('SET_SERVER_ID');

export default createReducer(initialState, builder => {
  builder.addCase(setConnectionStatus.type, (state, action: PayloadAction<ConnectionStatus>) => {
    state.status = action.payload;
  });
  builder.addCase(setServerId.type, (state, action: PayloadAction<string>) => {
    state.serverId = action.payload;
  });
});
import {
  ballBuilder,
  gameFinishedMessage,
  GameState,
  gameStatusChanged,
  gameUpdateMessage,
  Message,
  Paddle,
  paddleBuilder,
  Point2,
  Rect,
  Role,
  simulate,
  Status,
  User,
  Vector2,
} from '@curiosity/pong-common';

export type Session = {
  id: string;
  ws: WebSocket;
  user?: User;
};

export type Socket = {
  send: (message: Message<any>, recipient: string) => void;
  broadcastRoom: (message: Message<any>) => void;
  broadcast: (message: Message<any>) => void;
};

type SessionPlayer = {
  id: string;
  ready: boolean;
};

type Action = {
  t: number;
  paddle: Paddle;
  direction: Vector2;
};

export class Game {
  players: (SessionPlayer | undefined)[] = [undefined, undefined];
  status: Status = Status.OPEN;
  socket: Socket;
  gameLoop?: GameLoop;

  constructor(socket: Socket) {
    this.socket = socket;
  }

  setPlayerReady(index: number, ready: boolean) {
    const player = this.players[index];
    if (player) {
      player.ready = ready;
    }
  }

  join(playerId: string, role: Role) {
    if (role === Role.PLAYER_LEFT || role === Role.PLAYER_RIGHT) {
      this.players[role] = {
        id: playerId,
        ready: false,
      };
    }
  }

  leave(playerId: string) {
    const playerIndex = this.players.findIndex(
      (player) => player != null && player.id === playerId
    );
    if (playerIndex > -1) {
      this.players[playerIndex] = undefined;
      if (this.status === Status.IN_PROGRESS) {
        this.pause();
      }
    }
  }

  start() {
    const everyoneReady = this.players
      .map((p) => (p ? p.ready : false))
      .reduce((result, ready) => result && ready, true);
    if (everyoneReady) {
      this.gameLoop = new GameLoop(this.socket);
      this.status = Status.IN_PROGRESS;
      this.gameLoop.start();
    }
  }

  resume() {
    if (
      this.players[Role.PLAYER_LEFT] != null &&
      this.players[Role.PLAYER_RIGHT] != null
    ) {
      this.status = Status.IN_PROGRESS;
      this.gameLoop?.start();
    }
  }

  pause() {
    this.status = Status.PAUSED;
    this.gameLoop?.stop();
  }

  end() {
    this.status = Status.FINISHED;
    this.gameLoop?.stop();
    this.socket.broadcastRoom(gameFinishedMessage());
  }
}

class GameLoop {
  private gameState: GameState;
  private intervalId?: ReturnType<typeof setInterval>;
  private socket: Socket;
  private updateBuffer: Action[] = [];

  constructor(socket: Socket) {
    this.gameState = {
      t: Date.now(),
      arena: new Rect(new Point2(-200, -150), new Point2(200, 150)),
      ball: ballBuilder('ball'),
      paddles: [paddleBuilder('paddle0'), paddleBuilder('paddle1')],
      score: [0, 0],
    };
    this.socket = socket;
  }
  start() {
    this.socket.broadcastRoom(
      gameStatusChanged(Status.IN_PROGRESS, this.gameState)
    );
    setTimeout(() => {
      this.gameState.t = Date.now();
      this.intervalId = setInterval(() => {
        const now = Date.now();
        const delta = now - this.gameState.t;
        const keyFrames = simulate(
          this.applyUpdate(this.gameState, this.updateBuffer),
          delta
        );
        this.socket.broadcastRoom(gameUpdateMessage(keyFrames));

        this.gameState = keyFrames[keyFrames.length - 1];
      }, 50);
    }, 3000);
  }

  stop() {
    clearInterval(this.intervalId);
    this.socket.broadcastRoom(gameStatusChanged(Status.PAUSED, this.gameState));
  }

  receiveUpdate(t: number, index: number, direction: Vector2) {
    this.updateBuffer.push({
      t,
      paddle: this.gameState.paddles[index],
      direction: direction,
    });
  }

  applyUpdate(gameState: GameState, updates: Action[]): GameState {
    updates.forEach((action) => {
      const scale = (action.paddle.speed / 1000) * (action.t - gameState.t);
      // compensate for latency?
      action.paddle.position.add(action.direction.scale(scale));
      action.paddle.direction = action.direction.normalize();
    });
    updates.length = 0;
    return gameState;
  }
}

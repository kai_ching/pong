import winston from "winston";
const { combine, timestamp, prettyPrint, printf } = winston.format;

const lineFormat = printf(
  ({timestamp, level, service, label, message}) =>
    `${timestamp} | [${level.toUpperCase()}] | [${service}] | ${message}`
);

const rootLogger = winston.createLogger({
  level: "debug",
  format: combine(
    timestamp({format: "YYYY-MM-DD HH:mm:ss.SSS"}),
    prettyPrint(),
    lineFormat
  ),
  defaultMeta: {
    service: "Pong"
  },
  transports: [ new winston.transports.Console()]
});

export default rootLogger;

import {
  clientDisconnectedMessage,
  Message,
  MessageTypes,
  serverHelloMessage,
} from '@curiosity/pong-common';
import { WebSocketServer } from 'ws';
import { Session } from '../game';
import logger from '../logger.js';
import Lobby from './lobby';
import handlePing from "./handlePing";
import handleMessageRelay from "./handleMessageRelay";

type ClientWebSocket = WebSocket & { sessionId?: string };

const sessions: { [key: string]: Session } = {};
const SessionProvider =
  (sessions: { [key: string]: Session }) => (sessionId: string) =>
    sessions[sessionId];
export const getSession = SessionProvider(sessions);

const broadcast = (
  message: Message<any>,
  recipientId: string,
  senderId: string,
  sessionIds?: string[]
) => {
  const msg = message.pack(recipientId, senderId);
  (sessionIds
    ? sessionIds.map((sessionId) => getSession(sessionId))
    : Object.values(sessions)
  )
    .filter(
      (session) =>
        session &&
        session.id != senderId &&
        session.ws != null &&
        session.ws.readyState === 1
    )
    .forEach((session) => session.ws.send(msg));
};

export const send = (
  message: Message<any>,
  recipientId: string = serverId,
  senderId: string = serverId
) => {
  if (recipientId === serverId) {
    broadcast(message, recipientId, senderId);
  } else if (getSession(recipientId) != null) {
    const session = getSession(recipientId);
    if (session.ws && session.ws.readyState === 1) {
      session.ws.send(message.pack(recipientId, senderId));
    }
  } else {
    const room = lobby.findRoom(recipientId);
    if(room) {
      broadcast(message, recipientId, senderId, room.members);
    }
  }
};

const messageHandlers: {
  [key: number]: ((message: Message<any>) => boolean)[];
} = {};
export const registerMessageHandler = (
  messageType: MessageTypes,
  handler: (message: Message<any>) => boolean
) => {
  let existingHandlers = messageHandlers[messageType];
  if (!existingHandlers) {
    existingHandlers = [];
    messageHandlers[messageType] = existingHandlers;
  }
  existingHandlers.push(handler);
  return handler;
};
// const unregisterMessageHandler = (
//   messageType: MessageTypes,
//   handler: (message: Message<any>) => boolean
// ) => {
//   const handlers = messageHandlers[messageType];
//   if (handlers) {
//     const index = handlers.indexOf(handler);
//     if (index > -1) {
//       handlers.splice(index, 1);
//     }
//   }
// };

const serverId = '_' + Math.random().toString(36).substring(2, 10);
const lobby = new Lobby(serverId);
const websocketServer = new WebSocketServer({
  noServer: true,
  path: '/realtime',
});

const createSessionId = () => {
  let id: string | null = null;
  while (id == null || sessions[id] != null) {
    id = '@' + Math.random().toString(36).substring(2, 10);
  }
  return id;
};
const createSession = (ws: ClientWebSocket, request: Request): Session => {
  const id = createSessionId();
  ws.sessionId = id;
  const session = {
    id,
    ws,
  };
  sessions[id] = session;
  return session;
};

websocketServer.on('connection', (ws: WebSocket, request: Request) => {
  logger.info(`Client connected.`);
  const session = createSession(ws, request);
  session.ws.send(
    serverHelloMessage().pack(session.id, serverId)
  );

  ws.addEventListener('message', (event) => {
    const message = Message.unpack(event.data);
    messageHandlers[message.type]?.some((handler) => handler(message));
  });
  ws.addEventListener('close', () => {
    Object.values(sessions).forEach((session) => {
      if (session.ws.readyState === 3) {
        if (session.user) {
          send(clientDisconnectedMessage(session.id, session.user));
        }
        delete sessions[session.id];
      }
    });
    logger.info(`Client disconnected.`);
  });
});

registerMessageHandler(MessageTypes.PING, handlePing);
registerMessageHandler(MessageTypes.JOIN_ROOM, handleMessageRelay);
registerMessageHandler(MessageTypes.LEAVE_ROOM, handleMessageRelay);
registerMessageHandler(MessageTypes.CHAT, handleMessageRelay);

export default websocketServer;

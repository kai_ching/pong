import {
  clientConnectedMessage,
  ClientHello,
  Message,
} from '@curiosity/pong-common';
import { getSession, send } from './websocketServer';

export default (message: Message<ClientHello>): boolean => {
  const { senderId, payload } = message;
  const session = getSession(senderId!);
  const user = {
    name: payload?.name!,
  };
  session.user = user;

  console.log(`${user.name} connected`);
  send(clientConnectedMessage(senderId!, user), senderId);
  return true;
};

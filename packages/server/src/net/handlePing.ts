import { Message, pongMessage } from '@curiosity/pong-common';
import { send } from './websocketServer';

export default (message: Message<void>): boolean => {
  const { senderId, ts } = message;
  send(pongMessage(ts), senderId!);
  return true;
};

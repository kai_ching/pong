import { Message } from '@curiosity/pong-common';
import { send } from './websocketServer';

export default (message: Message<any>): boolean => {
  const { recipientId, senderId } = message;
  send(message, recipientId, senderId);
  return false;
};

import {
  Join,
  Message,
  MessageTypes
} from "@curiosity/pong-common";
import { Room } from './room';
import { registerMessageHandler } from "./websocketServer";
import logger from "../logger";

const recursiveFindRoom = (root: Room, id: string) => {
  if(root.id === id) {
    return root;
  }
  return Object.values(root.childrenRooms)
    .reduce((result, r) => result ? result : recursiveFindRoom(r, id), null);
}

export default class Lobby {
  private readonly room: Room;

  constructor(serverId: string) {
    this.room = new Room(serverId);

    registerMessageHandler(MessageTypes.OPEN_ROOM, (message: Message<void>) => {
      this.room.createSubRoom();
      logger.info("Room created");
      return false;
    });
    registerMessageHandler(
      MessageTypes.CLOSE_ROOM,
      (message: Message<string>) => {
        const roomId = message.payload;
        const room = recursiveFindRoom(this.room, roomId!);
        if(room) {
          room.close();
        }
        return false;
      }
    );
    registerMessageHandler(MessageTypes.JOIN_ROOM, (message: Message<Join>) => {
      const { senderId, payload } = message;
      const { roomId, name, role } = payload!;
      const room = recursiveFindRoom(this.room, roomId);
      if (room) {
        room.join(senderId, name, role);
      }
      return false;
    });
    registerMessageHandler(
      MessageTypes.LEAVE_ROOM,
      (message: Message<string>) => {
        const { senderId, payload } = message;
        const room = recursiveFindRoom(this.room, payload!);
        if (room) {
          room.leave(senderId!);
        }
        return false;
      }
    );
    registerMessageHandler(MessageTypes.PLAYER_ACTION, (message) => {
      const { recipientId } = message;
      const room = recursiveFindRoom(this.room, recipientId!);
      if (room) {
        room.onMessage(message);
      }
      return room != null;
    });
  }

  findRoom(roomId: string) {
    return recursiveFindRoom(this.room, roomId);
  }
}

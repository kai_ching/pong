import { lobbyUpdateMessage, LobbyUser, Message, Role, roomStatus } from "@curiosity/pong-common";
import {Socket } from '../game';
import { send } from './websocketServer';

type EventType = 'memberJoined' | 'memberLeft' | 'subRoomCreated' | 'roomClosed' | 'message';
type EventHandler = (socket: Socket, param?: Message<any> | LobbyUser | Room) => void;

const createRoomId = (): string => {
  return '#' + Math.random().toString(36).substring(2, 10);
}

export class Room {
  id: string;
  members: LobbyUser[] = [];
  childrenRooms: { [key: string]: Room } = {};

  private readonly socket: Socket;
  private readonly handlers: {[key: string]: EventHandler} = {}

  constructor(id: string) {
    this.id = id;
    this.socket = {
      send: (message: Message<any>, recipientId: string) =>
        send(message, recipientId, this.id),
      broadcastRoom: (message: Message<any>) => send(message, this.id, this.id),
      broadcast: (message: Message<any>) => send(message),
    };
  }

  addEventListener( eventType: EventType, handler: EventHandler) {
    this.handlers[eventType] = handler
  }

  onMessage(message: Message<any>) {
    if(typeof this.handlers['message'] === 'function') {
      this.handlers.message(this.socket, message);
    }
  }

  join(memberId: string, name: string, role: Role = Role.SPECTATOR) {
    let member = this.members.find(m => m.id === memberId);
    if(member) {
      member.name = name;
      member.role = role;
    } else {
      member = {
        id: memberId,
        name: name,
        role: role
      }
      this.members.push(member);
      console.log(`${name} joined ${this.id} as ${role}`);

      const childrenRooms = Object.values(this.childrenRooms).map(room => ({
        id: room.id,
        size: room.members.length,
        members: room.members
      }));

      const roomStatusMessage = roomStatus(this.members, childrenRooms);
      this.socket.send(roomStatusMessage, memberId)
    }
    if(typeof this.handlers.memberJoined === 'function') {
      this.handlers.memberJoined(this.socket, member);
    }
  }

  leave(memberId: string) {
    const index = this.members.findIndex(m => m.id === memberId);
    if(index > -1) {
      const member = this.members[index];
      this.members.splice(index, 1);
      if(typeof this.handlers.memberLeft === 'function') {
        this.handlers.memberLeft(this.socket, member);
      }
    }
  }

  close() {
    Object.values(this.childrenRooms).forEach(room => {
      room.close();
    });
    this.members.forEach(member => {
      this.leave(member.id);
    });

    if (typeof this.handlers['roomClosed'] === 'function') {
      this.handlers.roomClosed(this.socket);
    }
  }

  createSubRoom() {
    const id = createRoomId();
    const room = new Room(id);
    this.childrenRooms[id] = room;
    this.socket.broadcastRoom(lobbyUpdateMessage().roomAdded(room.id));
    if(typeof this.handlers['subRoomCreated'] === 'function') {
      this.handlers['subRoomCreated'](this.socket, room);
    }
    return room;
  }
}

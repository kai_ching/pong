import express from 'express';
import logger from './logger.js';
import websocketServer from './net/websocketServer.js';
import health from './health.js';


const app = express();
app.use(express.static('../client/dist'));
const port = process.env.PORT || 3000;

app.get('/health', health);

const expressServer = app.listen(port, () => {
  logger.info(`Server running on port ${port}`);
});

expressServer.on('upgrade', (request: Request, socket, head) => {
  websocketServer.handleUpgrade(request, socket, head, (websocket) => {
    websocketServer.emit('connection', websocket, request);
  });
});

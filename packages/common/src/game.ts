import { Rect } from './bounds';
import { Ball } from './entities/ball';
import { Paddle } from './entities/paddle';
import { Collision } from './physics';

export type User = {
  name: string;
};

export enum Role {
  PLAYER_LEFT,
  PLAYER_RIGHT,
  SPECTATOR,
}

export enum Status {
  OPEN,
  IN_PROGRESS,
  PAUSED,
  FINISHED,
}

export type GameState = {
  t: number;
  arena: Rect;
  ball: Ball;
  paddles: Paddle[];
  score: number[];
  collision?: Collision;
};

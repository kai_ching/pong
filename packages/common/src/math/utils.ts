export const EPSILON = 0.0005;

export const equals = (
  num1: number,
  num2: number,
  epsilon: number = EPSILON
): boolean => Math.abs(num1 - num2) < epsilon;

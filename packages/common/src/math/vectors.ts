import { Tuple } from './tuples';

export class Vector extends Tuple {
  normalize() {
    const length = this.length();
    this.components.forEach((value, index) => {
      this.components[index] = value / length;
    });
    return this;
  }

  dot(vector: Vector): number {
    return this.components.reduce(
      (result, value, index) => result + value * vector.components[index],
      0
    );
  }
}

export class Vector2 extends Vector {
  constructor(x: number = 0, y: number = 0) {
    super(x, y);
  }
}

export class Vector3 extends Vector {
  constructor(x: number = 0, y: number = 0, z: number = 0) {
    super(x, y, z);
  }

  cross(vector1: Vector3, vector2?: Vector3): Vector3 {
    let a, b, c, d, e, f: number;

    if (vector2) {
      [a, b, c] = vector1.components;
      [d, e, f] = vector2.components;
    } else {
      [a, b, c] = this.components;
      [d, e, f] = vector1.components;
    }
    this.components[0] = b * f - c * e;
    this.components[1] = c * d - a * f;
    this.components[2] = a * e - b * d;
    return this;
  }
}

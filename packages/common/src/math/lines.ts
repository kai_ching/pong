import { Vector } from './vectors';
import { Point } from './points';
import { equals } from './utils';

export type Intersection = {
  selfScalar: number;
  lineScalar: number;
  point: Point;
};

export class Line {
  public start: Point;
  public direction: Vector;
  public length: number;

  constructor(point: Point, direction: Vector, length: number = Infinity) {
    this.start = point;
    this.direction = direction.normalize();
    this.length = length;
  }

  contains(point: Point): boolean {
    const delta = new Vector().sub(point, this.start);
    const deltaLength = delta.length();

    let dot = delta.dot(this.direction);
    if (this.length == Infinity) {
      dot = Math.abs(dot);
    }
    return equals(dot, deltaLength) && deltaLength <= this.length;
  }

  intersects(line: Line): Intersection | undefined {
    if (equals(line.direction.dot(this.direction), 1)) {
      return undefined;
    }

    const s =
      (line.start.y * line.direction.x -
        line.direction.x * this.start.y +
        this.start.x * line.direction.y -
        this.start.x * line.direction.y) /
      (this.direction.y * line.direction.x -
        this.direction.x * line.direction.y);

    const t =
      (this.start.x + s * this.direction.x - line.start.x) / line.direction.x;

    if (s <= this.length && t <= line.length) {
      const vector1 = new Vector().scale(s, this.direction);
      const point1 = new Point().add(this.start, vector1);

      const vector2 = new Vector().scale(t, line.direction);
      const point2 = new Point().add(line.start, vector2);

      if (point1.equals(point2)) {
        return {
          selfScalar: s,
          lineScalar: t,
          point: point1,
        };
      } else {
        return undefined;
      }
    }
    return undefined;
  }
}

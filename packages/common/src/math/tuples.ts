import { equals as approxEquals } from './utils';

export class Tuple {
  public components: number[] = [];

  constructor(...values) {
    for (let i = 0; i < values.length; i++) {
      this.components.push(values[i]);
    }
  }

  get x() {
    return this.components[0];
  }
  set x(value: number) {
    this.components[0] = value;
  }
  get y() {
    return this.components[1];
  }
  set y(value: number) {
    this.components[1] = value;
  }
  set(...components: number[]): Tuple {
    components.forEach((value, index) => {
      this.components[index] = value;
    });
    return this;
  }

  copy(): this {
    return this.constructor(this.components);
  }

  equals(tuple): boolean {
    return this.components.reduce(
      (result, value, index) =>
        result && approxEquals(value, tuple.components[index]),
      true
    );
  }

  add(tuple1: Tuple, tuple2?: Tuple): this {
    if (tuple2) {
      this.components.length = tuple2.components.length;
      tuple2.components.forEach((value, index) => {
        this.components[index] =
          tuple1.components[index] + tuple2.components[index];
      });
    } else {
      this.components.forEach((value, index) => {
        this.components[index] += tuple1.components[index];
      });
    }
    return this;
  }

  sub(tuple1: Tuple, tuple2?: Tuple): this {
    if (tuple2) {
      this.components.length = tuple2.components.length;
      tuple2.components.forEach((value, index) => {
        this.components[index] =
          tuple1.components[index] - tuple2.components[index];
      });
    } else {
      this.components.forEach((value, index) => {
        this.components[index] -= tuple1.components[index];
      });
    }
    return this;
  }

  scale(scalar: number, tuple?: Tuple): this {
    if (tuple) {
      this.components.length = tuple.components.length;
      tuple.components.forEach((value, index) => {
        this.components[index] = tuple.components[index] * scalar;
      });
    } else {
      this.components.forEach((value, index) => {
        this.components[index] *= scalar;
      });
    }
    return this;
  }

  lengthSquared(): number {
    return this.components.reduce((result, value) => result + value * value, 0);
  }
  length = (): number => Math.sqrt(this.lengthSquared());
}

export class Tuple2 extends Tuple {
  constructor(x: number = 0, y: number = 0) {
    super(x, y);
  }
}

export class Tuple3 extends Tuple {
  constructor(x: number = 0, y: number = 0, z: number = 0) {
    super(x, y, z);
  }

  get z() {
    return this.components[3];
  }

  set z(value) {
    this.components[3] = value;
  }
}

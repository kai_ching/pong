import { Tuple } from './tuples';

export class Point extends Tuple {
  distance(point) {
    const result = this.components.reduce((result, value, index) => {
      const diff = point.components[index] - this.components[index];
      return result + diff * diff;
    }, 0);
    return Math.sqrt(result);
  }
}
export class Point2 extends Point {
  constructor(x: number = 0, y: number = 0) {
    super(x, y);
  }
}

export class Point3 extends Point {
  constructor(x: number = 0, y: number = 0, z: number = 0) {
    super(x, y, z);
  }
}

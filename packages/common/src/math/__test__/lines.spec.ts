import { Line } from '../lines';
import { Point2 } from '../points';
import { Vector2 } from '../vectors';

describe('Lines of infinite length', () => {
  // Line crosses origin
  const infiniteLine = new Line(new Point2(0, 0), new Vector2(1, 1));
  // Line crosses arbitrary point
  const infiniteLine2 = new Line(new Point2(3, 4), new Vector2(-1, 0));

  it('should contain point', () => {
    expect(infiniteLine.contains(new Point2(2, 2))).toBe(true);
    expect(infiniteLine.contains(new Point2(-2, -2))).toBe(true);
    expect(infiniteLine2.contains(new Point2(4, 4))).toBe(true);
    expect(infiniteLine2.contains(new Point2(-4, 4))).toBe(true);
  });

  it('should not contain point', () => {
    expect(infiniteLine.contains(new Point2(0, 1))).toBe(false);
    expect(infiniteLine2.contains(new Point2(4, 5))).toBe(false);
  });
});

describe('Lines of finite length', () => {
  const finiteLine = new Line(new Point2(2, 3), new Vector2(-1, -0), 2);

  it('should contain point', () => {
    expect(finiteLine.contains(new Point2(1, 3))).toBe(true);
    expect(finiteLine.contains(new Point2(0, 3))).toBe(true);
  });

  it('should not contain point', () => {
    expect(finiteLine.contains(new Point2(-1, 3))).toBe(false);
    expect(finiteLine.contains(new Point2(3, 3))).toBe(false);
  });

  it('should intersect', () => {
    const line1 = new Line(new Point2(-1, 1), new Vector2(1, -1), Math.sqrt(8));
    const line2 = new Line(new Point2(-1, -1), new Vector2(1, 1), Math.sqrt(8));
    const intersection = line1.intersects(line2);

    expect(intersection).toBeDefined();
    expect(intersection?.selfScalar).toBe(Math.sqrt(2));
    expect(intersection?.lineScalar).toBe(Math.sqrt(2));
    expect(intersection?.point.equals(new Point2(0, 0))).toBe(true);
  });
});

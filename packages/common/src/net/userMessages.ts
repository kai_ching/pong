import { Role } from '../game';
import { Message, MessageTypes } from './messages';

export type Join = {
  roomId: string;
  name: string;
  role: Role;
};

export type Chat = {
  authorId: string;
  recipientId: string;
  content: string;
};

export const joinMessage = (roomId: string, name: string, role: Role) =>
  new Message<Join>(MessageTypes.JOIN_ROOM).withPayload({
    roomId,
    name,
    role,
  });

export const leaveMessage = (senderId: string, roomId: string) => {
  new Message<string>(MessageTypes.LEAVE_ROOM).withPayload(roomId);
};

export const chatMessage = (
  senderId: string,
  authorId: string,
  recipientId: string,
  content: string
) => {
  new Message<Chat>(MessageTypes.CHAT).withPayload({
    authorId,
    recipientId,
    content,
  });
};

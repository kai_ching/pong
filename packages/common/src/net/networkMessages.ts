import { User } from '../game';
import { Message, MessageTypes } from './messages';


export type ServerHello = {
};

export type UserSummary = {
  id: string;
  user: {
    name: string;
  };
};

export const pingMessage = () => new Message<void>(MessageTypes.PING);
export const pongMessage = (pingTs: number) =>
  new Message<number>(MessageTypes.PONG).withPayload(pingTs);
export const serverHelloMessage = () =>
  new Message<ServerHello>(MessageTypes.SERVER_HELLO);
export const clientConnectedMessage = (id: string, user: User) =>
  new Message<UserSummary>(MessageTypes.CLIENT_CONNECTED).withPayload({
    id,
    user,
  });
export const clientDisconnectedMessage = (id: string, user: User) =>
  new Message<UserSummary>(MessageTypes.CLIENT_DISCONNECTED).withPayload({
    id,
    user,
  });

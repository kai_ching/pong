export enum MessageTypes {
  PING,
  PONG,
  CLIENT_CONNECTED,
  CLIENT_DISCONNECTED,
  SERVER_HELLO,
  CLIENT_HELLO,

  LOBBY_UPDATES,
  ROOM_STATUS,
  OPEN_ROOM,
  CLOSE_ROOM,

  JOIN_ROOM,
  LEAVE_ROOM,
  CHAT,

  PLAYER_ACTION,
  GAME_STATUS_CHANGED,
  GAME_UPDATE,
  GAME_FINISHED,
}

export class Message<T> {
  type: MessageTypes;
  ts: number = -1;
  payload?: T;
  senderId?: string;
  recipientId?: string;

  static unpack<T>(data: string): Message<T> {
    const [recipientId, senderId, ts, type, payload] = JSON.parse(data);
    const msg = new Message(type);
    msg.ts = ts;
    msg.recipientId = recipientId;
    msg.senderId = senderId;
    msg.payload = payload;
    return msg as Message<T>;
  }

  constructor(type: MessageTypes) {
    this.type = type;
  }

  pack(recipientId: string, senderId: string): string | ArrayBuffer {
    return JSON.stringify([
      recipientId,
      senderId,
      Date.now(),
      this.type,
      this.payload,
    ]);
  }

  withPayload(payload: T): Message<T> {
    this.payload = payload;
    return this;
  }
}

import { Message, MessageTypes } from "./messages";
import { Role } from "../game";

export type LobbyUser = {
  id: string;
  name: string;
  role: Role;
}

export type RoomSummary = {
  id: string;
  size: number;
  members?: LobbyUser[];
}

export type RoomStatus = {
  id?: string;
  members: LobbyUser[];
  rooms: RoomSummary[];
}

class LobbyMessages extends Message<LobbyChanges[]> {
  constructor() {
    super(MessageTypes.LOBBY_UPDATES);
    this.payload = [];
  }
  roomAdded(roomId: string) {
    this.payload?.push({
      action: Actions.ADD,
      room: { id: roomId },
    });
    return this;
  }
  roomRemoved(roomId: string) {
    this.payload?.push({
      action: Actions.REMOVE,
      room: { id: roomId },
    });
    return this;
  }
}

export enum Actions {
  ADD,
  REMOVE,
}

export type LobbyChanges = {
  action: Actions;
  room: {
    id: string;
  };
};

export const lobbyUpdateMessage = () => new LobbyMessages();

export const roomStatus = (members: LobbyUser[], children: RoomSummary[]) => {
  return new Message<RoomStatus>(MessageTypes.ROOM_STATUS).withPayload({
    members: members,
    rooms: children
  });
}

export const openRoomMessage = () => {
  return new Message<void>(MessageTypes.OPEN_ROOM);
};

export const closeRoomMessage = (roomId: string) => {
  return new Message<string>(MessageTypes.CLOSE_ROOM).withPayload(roomId);
};

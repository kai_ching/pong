import { GameState, Status } from '../game';
import { Vector2 } from '../math/vectors';
import { Message, MessageTypes } from './messages';

export type GameStatusChange = {
  status: Status;
  gameState: GameState;
};

export const playerReadyMessage = (ready: boolean) =>
  new Message(MessageTypes.PLAYER_ACTION).withPayload({ ready });
export const playerDirectionMessage = (direction: Vector2) =>
  new Message(MessageTypes.PLAYER_ACTION).withPayload({ direction });
export const gameStatusChanged = (status: Status, gameState: GameState) =>
  new Message<GameStatusChange>(MessageTypes.GAME_STATUS_CHANGED).withPayload({
    status,
    gameState,
  });
export const gameUpdateMessage = (keyFrames: GameState[]) =>
  new Message<GameState[]>(MessageTypes.GAME_UPDATE).withPayload(keyFrames);

export const gameFinishedMessage = () =>
  new Message<void>(MessageTypes.GAME_FINISHED);

import { Point2 } from './math/points';
import { Line } from './math/lines';
import { Vector2 } from './math/vectors';

abstract class Bounds {
  position: Point2 = new Point2();
}

export class Rect extends Bounds {
  p1: Point2;
  p2: Point2;

  constructor(p1: Point2, p2: Point2) {
    super();
    this.p1 = p1;
    this.p2 = p2;
  }

  getEdges(): Line[] {
    const width = Math.abs(this.p2.x - this.p1.x);
    const height = Math.abs(this.p1.y - this.p2.y);

    const p1 = new Point2();
    p1.add(this.p1, this.position);
    const p2 = new Point2();
    p2.add(this.p2, this.position);

    return [
      new Line(p1, new Vector2(1, 0), width),
      new Line(new Point2(p2.x, p1.y), new Vector2(0, -1), height),
      new Line(p2, new Vector2(-1, 0), width),
      new Line(new Point2(p1.x, p2.y), new Vector2(0, 1), height),
    ];
  }
}

export class Circle extends Bounds {
  radius: number;

  constructor(radius: number) {
    super();
    this.radius = radius;
  }
}

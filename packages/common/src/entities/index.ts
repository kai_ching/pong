
export type Entity = {
  readonly id: string;
}
class EntityManager {

  private entities: { [key: string] : any } = {}

  createEntity<T extends Entity>(entityBuilder: (id: string) => T) : T {
    let id;
    while(!id || this.entities[id]) {
      id =  Math.random().toString(36).substring(2);
    }
    return entityBuilder(id);
  }

  removeEntity(id: string) {
    if(this.entities[id]) {
      delete this.entities[id];
    }
  }
}

export const entityManager = new EntityManager();
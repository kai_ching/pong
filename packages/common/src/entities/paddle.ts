import { Rect } from '../bounds';
import { Point2 } from '../math/points';
import { Vector2 } from '../math/vectors';

export type Paddle = {
  position: Vector2;
  direction: Vector2;
  bounds: Rect;
  speed: number;
};

export const paddleBuilder = (id: string): Paddle => {
  return {
    position: new Vector2(),
    direction: new Vector2(),
    bounds: new Rect(new Point2(-1, -2), new Point2(1, 2)),
    speed: 3,
  };
};

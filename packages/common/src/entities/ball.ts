import { Circle } from '../bounds';
import { Point2 } from '../math/points';
import { Vector2 } from '../math/vectors';
import { Entity } from './index';

export type Ball = {
  position: Point2;
  direction: Vector2;
  speed: number;
  bounds: Circle;
} & Entity;

export const ballBuilder = (id: string): Ball => {
  return {
    id: id,
    position: new Point2(),
    direction: new Vector2(),
    bounds: new Circle(3),
    speed: 5,
  };
};

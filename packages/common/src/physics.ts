import { produce } from 'immer';
import { Circle, Rect } from './bounds';
import { GameState, Role } from './game';
import { Line } from './math/lines';
import { Point, Point2 } from './math/points';
import { Vector2 } from './math/vectors';

export type Collision = {
  t: number;
  point: Point;
  line: Line;
};

const collideWithRect = (
  position: Point2,
  velocity: Vector2,
  ballBounds: Circle,
  rectBounds: Rect,
  t: number = 1
): Collision | undefined => {
  let result: Collision | undefined;
  const offset = velocity.copy().scale(ballBounds.radius / velocity.length());
  const contactPoint = position.copy().add(offset);
  const motionLine = new Line(contactPoint, velocity, velocity.length() * t);
  rectBounds.getEdges().some((line) => {
    const intersection = line.intersects(motionLine);
    if (intersection != null) {
      result = {
        t: intersection.lineScalar,
        point: intersection.point,
        line: line,
      };
    }
    return intersection != null;
  });
  return result;
};

const updateGameState = (
  gameState: GameState,
  delta: number,
  collision: Collision | undefined
): GameState => {
  const nextGameState = produce(gameState, (draft) => {
    draft.paddles.forEach((paddle) => {
      const velocity = new Vector2().scale(paddle.speed, paddle.direction);
      paddle.position.add(velocity);
    });

    if (collision) {
      const { t, line } = collision;
      draft.t += t;
      draft.ball.position = collision.point;
      draft.collision = collision;
      if (line.direction.x == 0) {
        // horizontal collision
        draft.ball.direction.y *= -1;
      } else if (line.direction.y == 0) {
        // vertical collision
        draft.ball.direction.x *= -1;
      }
    } else {
      draft.t += delta;
      const velocity = new Vector2().scale(
        draft.ball.speed,
        draft.ball.direction
      );
      draft.ball.position.add(velocity);
    }
  });
  return nextGameState;
};

export const simulate = (
  gameState: GameState,
  delta: number,
  keyFrames: GameState[] = []
): GameState[] => {
  const { position, direction, speed } = gameState.ball;
  let collision;

  if (delta > 0) {
    // collide with arena;
    collision = collideWithRect(
      position,
      direction.copy().scale(speed),
      gameState.ball.bounds,
      gameState.arena,
      delta
    );
    if (collision == null) {
      // collide with paddle
      // TODO: what if paddles are allowed to move forward?
      if (direction.x < 0) {
        collision = collideWithRect(
          position,
          direction.copy().scale(speed),
          gameState.ball.bounds,
          gameState.paddles[Role.PLAYER_LEFT].bounds,
          delta
        );
      } else {
        collision = collideWithRect(
          position,
          direction.copy().scale(speed),
          gameState.ball.bounds,
          gameState.paddles[Role.PLAYER_RIGHT].bounds,
          delta
        );
      }
    }

    const nextGameState = updateGameState(gameState, delta, collision);
    keyFrames.push(nextGameState);
    if (collision != null) {
      simulate(nextGameState, delta - collision.t, keyFrames);
    }
  }
  return keyFrames;
};
